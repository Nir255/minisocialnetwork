var RegisterForm = React.createClass({
    getInitialState() {
        return ({ username: "", firstname: "", lastname: "", password: "", email: "", about: "", image: null });
    },
    handlefirstname(event) {
        this.setState({ firstname: event.target.value });
    },
    handlelastname(event) {
        this.setState({ lastname: event.target.value });
    },
    handleUsername(event) {
        this.setState({ username: event.target.value });
    },
    handleEmail(event) {
        this.setState({ email: event.target.value });
    },
    handlePassword(event) {
        this.setState({ password: event.target.value });
    },
    handleAbout(event) {
        this.setState({ about: event.target.value });
    },
    handleImage(event) {
        this.setState({ image: event.target.files[0] });
    },
    handleSubmit(event) {
        var formData = new FormData(); //posting in multipart-form because uploading image.
        for (var field in this.state) {
            formData.append(field, this.state[field]);
        };
        event.preventDefault();
        fetch('/register', {
            method: 'POST',
            body: formData
        }).then((res) => {
            if (res.status == 200) {
                location.reload(); //if registration successful user has cookie and reloading moving him to main users page.
            }
            else {
                return res.json()
            }
        }).then(err => {
            console.log(err)
            if (err.message == "SQLITE_CONSTRAINT: UNIQUE constraint failed: users.Username") {
                this.setState({ error: "Username already exist" });
            }
            else if (err.message == "SQLITE_CONSTRAINT: UNIQUE constraint failed: users.Email") {
                this.setState({ error: "Email already exist" })
            }
        });
    },
    render() {
        return (
            <div className="card mt-2 mb-2">
                <h5 className="card-header info-color py-4 d-flex row">
                    <a className="mr-auto" onClick={this.props.handleback}><i className="fas fa-backward"></i></a>
                    <strong className="white-text text-left col-11">Sign up</strong>
                </h5>
                <div className="card-body px-lg-5 pt-0">
                    <form className="text-center" style={{ color: '#757575' }} onSubmit={this.handleSubmit}>
                        <div className="form-row mt-0">
                            <div className="col">
                                <div className="md-form">
                                    <label htmlFor="materialRegisterFormfirstname">Profile Image</label>
                                </div>
                            </div>
                            <div className="col">
                                <div className="md-form">
                                    <input type="file" id="materialRegisterImage" className="hidden" accept="image/*" onChange={this.handleImage} />
                                </div>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="col">
                                <div className="md-form">
                                    <input type="text" id="materialRegisterFormfirstname" className="form-control" onChange={this.handlefirstname} required />
                                    <label htmlFor="materialRegisterFormfirstname">First name</label>
                                </div>
                            </div>
                            <div className="col">
                                <div className="md-form">
                                    <input type="text" id="materialRegisterFormlastname" className="form-control" onChange={this.handlelastname} required />
                                    <label htmlFor="materialRegisterFormlastname">Last name</label>
                                </div>
                            </div>
                        </div>
                        <div className="md-form mt-0">
                            <input type="text" id="materialRegisterFormUsername" className="form-control" onChange={this.handleUsername} required />
                            <label htmlFor="materialRegisterFormUsername">Username</label>
                        </div>
                        <div className="md-form">
                            <input type="email" id="materialRegisterFormEmail" className="form-control" onChange={this.handleEmail} required />
                            <label htmlFor="materialRegisterFormEmail">E-mail</label>
                        </div>
                        <div className="md-form mt">
                            <input type="text" id="materialRegisterAbout" className="form-control" onChange={this.handleAbout} required />
                            <label htmlFor="materialRegisterFormAbout">Hobbies</label>
                        </div>
                        <div className="md-form">
                            <input type="password" id="materialRegisterFormPassword" className="form-control" pattern=".{6,}" aria-describedby="materialRegisterFormPasswordHelpBlock" onChange={this.handlePassword} required />
                            <label htmlFor="materialRegisterFormPassword">Password</label>
                            <small id="materialRegisterFormPasswordHelpBlock" className="form-text text-muted mb-4">
                                At least 6 characters
                            </small>
                        </div>
                        <button className="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit">Sign Up</button>
                        <hr />
                        <p>By clicking
                            <em> Sign up</em> you agree to our
                            <a href="" target="_blank"> terms of service</a>
                        </p>
                    </form>
                    {this.state.error ? <h7 className="text-danger font-weight-bold">{this.state.error}</h7> : null}
                </div>
            </div>
        )
    }
});

var LoginForm = React.createClass({
    getInitialState() {
        return ({ username: "", password: "", incorrect: 0 });
    },

    handleUsernameChange(event) {
        this.setState({ username: event.target.value });
    },

    handlePasswordChange(event) {
        this.setState({ password: event.target.value });
    },

    handleSubmitLogin(event) {
        event.preventDefault();
        fetch('/login', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password
            })
        }).then((res) => {
            if (res.status == 200) {
                this.setState({ currentMessage: "" })
                window.location.reload();
            }
            else if (res.status == 401) {
                this.setState({ incorrect: 1 });
            }
        })

    },

    render() {
        return (
            <div className="card mt-2 mb-2">
                <h5 className="card-header info-color py-4 d-flex row">
                    <a className="mr-auto" onClick={this.props.handleback}><i className="fas fa-backward"></i></a>
                    <strong className="white-text text-left col-11">Sign in</strong>
                </h5>
                <div className="card-body px-lg-5 pt-0">
                    <form className="text-center" style={{ color: '#757575' }} action="#!">
                        <div className="md-form">
                            <input type="text" id="materialLoginFormUsername" className="form-control" onChange={this.handleUsernameChange} required />
                            <label htmlFor="materialLoginFormUsername">Username</label>
                        </div>
                        <div className="md-form">
                            <input type="password" id="materialLoginFormPassword" className="form-control" onChange={this.handlePasswordChange} required />
                            <label htmlFor="materialLoginFormPassword">Password</label>
                        </div>
                        <div className="d-flex justify-content-around">
                            <div>
                                <div className="form-check">
                                    <input type="checkbox" className="form-check-input" id="materialLoginFormRemember" />
                                    <label className="form-check-label" htmlFor="materialLoginFormRemember">Remember me</label>
                                </div>
                            </div>
                        </div>
                        <button className="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" onClick={this.handleSubmitLogin} type="submit">Sign in</button>
                    </form>
                    {this.state.incorrect ? <h7 className="text-danger font-weight-bold">Incorrect</h7> : null}
                </div>
            </div>
        );
    }
});

var Intro = React.createClass({
    render() {
        return (
            <div className="col-md-12 mb-4 white-text text-center">
                <h1 className="h1-reponsive white-text text-uppercase font-weight-bold mb-0 pt-md-5 pt-5"><strong>Mini Social Network</strong></h1>
                <hr className="hr-light my-4" />
                <h5 className="mb-4 white-text"><strong>Nir Abramovich</strong></h5>
                <button className="btn btn-outline-white" onClick={this.props.handlelogin}>Login</button>
                <button className="btn btn-outline-white" onClick={this.props.handleregister}>Register</button>
            </div>
        );
    }
});

var LoginOrRegister = React.createClass({ //when guest presses login/register in main guest page, redirecting to the right form
    getInitialState() {
        return ({ intro: 1, login: 0 })
    },

    handleLogin() {
        this.setState({ intro: 0, login: 1 });
    },

    handleBack() {
        this.setState({ intro: 1 });
    },

    handleRegister() {
        this.setState({ intro: 0, login: 0 });
    },

    render() {
        if (this.state.intro == 1) {
            return (
                <Intro handlelogin={this.handleLogin} handleregister={this.handleRegister} />
            );
        }
        else if (this.state.intro == 0 && this.state.login == 1) {
            return (
                <LoginForm handleback={this.handleBack} />
            );
        }
        else {
            return (
                <RegisterForm handleback={this.handleBack} />
            );
        }
    }
});

var MainGuest = React.createClass({
    render() {
        return (
            <div className="mask rgba-black-light align-items-center">
                <div className="container">
                    <div className="row d-flex justify-content-center" id="formDiv">
                        <LoginOrRegister />
                    </div>
                </div>
            </div>
        );
    }
});

ReactDOM.render(<MainGuest />, document.getElementById("guest"));