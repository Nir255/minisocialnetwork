var Layout = React.createClass({
    getInitialState() {
        return ({ searchTerm: "", friends: true, search: false, chat: false, searchData: [], otherSide: "", friendsData: [], friendsDataExist: false, friendsRequests: [] })
    },
    handleLogout() {
        location.href = '/logout'
    },
    handleSearchTerm(event) {
        this.setState({ searchTerm: event.target.value });
    },
    handleFriends() { //changes page to all friends
        this.setState({ friends: true, search: false, chat: false });
    },
    handleSearch(event) { //fetching peoples card by search term
        if (event) { event.preventDefault(); }
        if (this.state.searchTerm != "") {
            fetch("/api/search/" + this.state.searchTerm).then(response => response.json())
                .then(data => {
                    this.setState({ searchData: data, search: true, friends: false });
                });
        }
        else {
            this.handleFriends();
        }
    },
    handleChat(otherside, fullname) { //change page to chat with "otherside"
        this.setState({ friends: false, search: false, chat: true, otherSideUsername: otherside, othersideFullname: fullname });
    },
    fetchFriends() { //fetching all friends of user
        fetch("/api/friendsList/").then(response => response.json())
            .then(data => this.setState({ friendsData: data, friendsDataExist: true }));
    },
    fetchRequests() { //fetching all friend requests for user
        fetch("/api/friendsRequests/").then(response => response.json())
            .then(data => this.setState({ friendsRequests: data }));
    },
    reloadFriendsPage() {
        this.fetchFriends();
        this.fetchRequests();
    },
    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-light bg-light justify-content-between">
                    <a className="navbar-brand px-2" href="#">Mini Social Network</a>
                    <form className="form-inline my-2 my-lg-0 px-2" onSubmit={this.handleSearch}>
                        <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" onChange={this.handleSearchTerm} />
                        <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </form>
                    <button className="btn btn-outline-danger my-2 my-sm-0 px-2" type="submit" onClick={this.handleLogout}>Logout</button>
                </nav>
                <hr />
                <div id="content">
                    {this.state.friends ? <FriendsList chat={this.handleChat} reload={this.reloadFriendsPage} friendsData={this.state.friendsData} friendsDataExist={this.state.friendsDataExist} requests={this.state.friendsRequests} /> : null}
                    {this.state.search ? <SearchList searchData={this.state.searchData} backToFriends={this.handleFriends} chat={this.handleChat} reload={this.handleSearch} /> : null}
                    {this.state.chat ? <Chat othersideUsername={this.state.otherSideUsername} othersideFullname={this.state.othersideFullname} backToSearch={this.handleSearch} backToFriends={this.handleFriends} /> : null}
                </div>
                <footer className="page-footer font-small blue pt-4">
                    <div className="footer-copyright text-center py-3">© 2020 Copyright: Nir Abramovich</div>
                </footer>
            </div>
        );
    }
})

ReactDOM.render(<Layout />, document.getElementById("app"));