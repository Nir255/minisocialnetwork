/*Card for each friend */
var FriendCard = React.createClass({
    handleChat() { //goes to chat with user
        var fullname = this.props.firstname + " " + this.props.lastname;
        this.props.chat(this.props.username, fullname);
    },

    handleSending() { //sending friend request
        fetch('/api/friendRequest/', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                type: "send",
                otherside: this.props.username
            })
        }).then((res) => {
            if (res.status == 200) {
                this.props.reloadSearch(null);
            }
        });
    },

    handleAcceptingRequest() { //accepting friend request
        fetch('/api/friendRequest/', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                type: "accept",
                otherside: this.props.username
            })
        }).then((res) => {
            if (res.status == 200) {
                if (this.props.reloadSearch === undefined) {
                    this.props.reloadFriends();
                }
                else {
                    this.props.reloadSearch(null);
                }
            }
        });
    },

    handleDenyingRequset() { //deny friend request
        fetch('/api/friendRequest/', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                type: "deny",
                otherside: this.props.username
            })
        }).then((res) => {
            if (res.status == 200) {
                if (this.props.reloadSearch === undefined) {
                    this.props.reloadFriends();
                }
                else {
                    this.props.reloadSearch(null);
                }
            }
        });
    },

    handleUnfriend() {
        fetch('/api/friendRequest/', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                type: "deny",
                otherside: this.props.username
            })
        }).then((res) => {
            if (res.status == 200) {
                this.props.reloadFriends();
            }
        });
    },

    onHoverWhenFriends(event) { //shows "UNFRIEND" when hovering "FRIENDS!" button
        event.target.className = "btn btn-danger btn-rounded";
        event.target.innerText = "Unfriend";
    },

    unhoverWhenFriends(event) { //shows "FRIENDS!" again when stop hovering over "UNFRIEND" button
        event.target.className = "btn btn-light btn-rounded";
        event.target.innerHTML = 'Friends <svg class="svg-inline--fa fa-check fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg>';
    },

    whenFriendFooter() { // When friends: shows you are friends and also chat option
        return (
            <div className="row mb-2 justify-content-around">
                <button type="button" className="btn btn-light btn-rounded" onMouseEnter={this.onHoverWhenFriends} onMouseLeave={this.unhoverWhenFriends} onClick={this.handleUnfriend}>Friends <i className="fas fa-check"></i></button>
                <button type="button" className="btn btn-primary btn-rounded" onClick={this.handleChat}>Chat <i className="far fa-comment"></i></button>
            </div>
        );
    },

    whenNotFriendsFooter() { //when not friends: shows option to ask for friendship
        return (
            <div className="row mb-2 justify-content-around">
                <button type="button" className="btn btn-light btn-rounded" onClick={this.handleSending}>Ask for Friendship</button>
            </div>
        );
    },

    whenRequestPending() { //when user sent friendship request to another user
        return (
            <div className="row mb-2 justify-content-around">
                <button type="button" className="btn btn-light btn-rounded">Request Sent</button>
            </div>
        );
    },

    whenYouAreRequested() { //when user has friendship request from another user. Can accept or deny.
        return (
            <div className="row mb-2 justify-content-around">
                <button type="button" className="btn btn-rounded btn-success" onClick={this.handleAcceptingRequest}>Accept Request</button>
                <button type="button" className="btn btn-rounded btn-danger" onClick={this.handleDenyingRequset}>Deny Request</button>
            </div>
        );
    },

    staticTop() { // Most of users cards. static.
        var fullName = this.props.firstname + " " + this.props.lastname;
        return (
            <div className="d-flex justify-content-center">
                <div className="avatar mx-auto white">
                    <img src={"/images/" + this.props.photoId} className="rounded-circle mt-2" style={{ maxHeight: 100, maxWidth: 100 }} />
                </div>
                <div className="card-body">
                    <h4 className="card-title">{fullName}</h4>
                    <hr />
                    <p>{this.props.about}</p>
                </div>
            </div>
        );
    },

    render() {
        if (this.props.status == 0) {
            if (this.props.sender == Cookies.get("username")) {
                return (
                    <div className="card-container col-3 mt-2 mb-2">
                        <div className="card testimonial-card offset-1 col-10">
                            {this.staticTop()}
                            {this.whenRequestPending()}
                        </div>
                    </div>
                );
            }
            else {
                return (
                    <div className="card-container col-3 mt-2 mb-2">
                        <div className="card testimonial-card offset-1 col-10">
                            {this.staticTop()}
                            {this.whenYouAreRequested()}
                        </div>
                    </div>
                );
            }
        }
        else if (this.props.status == 1) {
            return (
                <div className="card-container col-3 mt-2 mb-2">
                    <div className="card testimonial-card offset-1 col-10">
                        {this.staticTop()}
                        {this.whenFriendFooter()}
                    </div>
                </div>
            );
        }
        else {
            return (
                <div className="card-container col-3 mt-2 mb-2">
                    <div className="card testimonial-card offset-1 col-10">
                        {this.staticTop()}
                        {this.whenNotFriendsFooter()}
                    </div>
                </div>
            );
        }
    }
});

/*Fetching friends data from api and representing*/
var FriendsList = React.createClass({
    componentDidMount() {
        this.props.reload();
    },

    render() {
        if (this.props.friendsDataExist && this.props.friendsData.length == 0) {
            var friendList = <h1>You have no friends yet. Start searching them and add them!</h1>;
        }
        else if (!this.props.friendsDataExist) {
            var friendList = <h1></h1>
        }
        else {
            friendList = this.props.friendsData.map((friend) =>
                <FriendCard key={friend.Username} username={friend.Username} firstname={friend.FirstName} lastname={friend.LastName} photoId={friend.PhotoId} about={friend.About} chat={this.props.chat} status={1} reloadFriends={this.props.reload} />
            )
        }
        var friendsRequests = <h1>None</h1>;
        if (this.props.requests && this.props.requests.length != 0) {
            friendsRequests = this.props.requests.map((request) =>
                <FriendCard key={request.Username} username={request.Username} firstname={request.FirstName} lastname={request.LastName} photoId={request.PhotoId} about={request.About} chat={this.props.chat} status={request.Confirmed} reloadFriends={this.props.reload} />
            )
        }
        return (
            <div>
                <h1 className="row offset-1">Pending Requests:</h1>
                <div className="row d-flex justify-content-around">
                    {friendsRequests}
                </div>
                <hr />
                <h1 className="row offset-1">Friends:</h1>
                <div className="row d-flex justify-content-around">
                    {friendList}
                </div>
            </div>
        );
    }
});