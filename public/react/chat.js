var SendBar = React.createClass({
    getInitialState() {
        return ({ currentMessage: "" });
    },

    handleSend(event) {
        if (this.state.currentMessage != "") {
            fetch('/api/chat/' + this.props.otherside, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    content: this.state.currentMessage,
                })
            }).then((res) => {
                if (res.status == 200) {
                    this.setState({ currentMessage: "" }) //reseting the line with the user written message
                }
            })
        }
    },

    handleChange(event) {
        this.setState({ currentMessage: event.target.value });
    },

    handleKeyDown(event) { //when pressing enter call send method
        if (event.key === 'Enter') {
            this.handleSend();
        }
    },

    render() {
        return (
            <div className="row d-flex align-items-center justify-content-center">
                <div className="col-11">
                    <input type="text" className="form-control" value={this.state.currentMessage} onKeyDown={this.handleKeyDown} placeholder="Type text to send" onChange={this.handleChange} />
                </div>
                <div className="col-1">
                    <button className="btn" onClick={this.handleSend}><i className="fa fa-paper-plane"></i></button>
                </div>
            </div>
        );
    }
});

var Message = React.createClass({
    myselfMessage() { //shows my messages, just like whatsapp in green
        return (
            <div className="offset-2 col-3">
                <div className="card bg-success">
                    <div className="card-body text-left text-light">
                        <p>{this.props.content}</p>
                    </div>
                    <p className="card-subtitle text-muted text-center">{this.props.time}</p>
                </div>
            </div>
        );
    },

    otherMessage() { //shows others messages, just like whatsapp in grey
        return (
            <div className="offset-7 col-3">
                <div className="card bg-light">
                    <div className="card-body text-right">
                        <p>{this.props.content}</p>
                    </div>
                    <p className="card-subtitle text-muted text-center">{this.props.time}</p>
                </div>
            </div>
        );
    },

    render() {
        if (this.props.sender == 0) { //displays by comparing the sender to cookie username
            var content = this.otherMessage();
        }
        else {
            var content = this.myselfMessage();
        }
        return (
            <div className="row mb-2">
                {content}
            </div>
        );
    }
});

var Chat = React.createClass({
    getInitialState() {
        return ({ messages: [] })
    },

    updateMessages() {
        fetch("/api/chat/" + this.props.othersideUsername).then(response => response.json()) //fetching chat messages from server
            .then(data => {
                if (data.length != 0) {
                    this.setState({ messages: data[0] })
                }
            });
    },

    componentDidMount() {
        this.interval = setInterval(() => this.updateMessages(), 1000); //refreshing chat every 1 sec
    },

    componentWillUnmount() {
        clearInterval(this.interval); //stops the chat refreshing timer when user left the chat page
    },

    handleBack() {
        this.props.backToFriends();
    },

    render() {
        if (this.state.messages.length == []) {
            var messages = <h1>Empty</h1>;
        }

        else {
            var messages = this.state.messages.map(message => {
                var senderMyself = 0;
                if (message.User1 == Cookies.get("username")) { //compares message sender to the username in the cookie; 1=true
                    senderMyself = 1;
                }
                return <Message sender={senderMyself} time={message.Time} content={message.Content} />;
            });
        }

        return (
            <div className="offset-2 col-8">
                <div className="row">
                    <button className="btn" onClick={this.handleBack}><i className="fas fa-backward"></i></button>
                    <h1>Chat with {this.props.othersideFullname}</h1>
                </div>
                <div style={{ height: 500, maxHeight: 500, overflowY: "auto", overflowX: "hidden" }}>
                    {messages}
                </div>
                <SendBar otherside={this.props.othersideUsername} />
            </div>
        );
    }
});