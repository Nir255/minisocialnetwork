var SearchList = React.createClass({
    render(){
        if (this.props.searchData.length!=0){
            searchList = this.props.searchData.map((someone)=>
            <FriendCard key={someone.Username} username={someone.Username} firstname={someone.FirstName} lastname={someone.LastName} photoId={someone.PhotoId} about={someone.About} chat={this.props.chat} status={someone.Confirmed} sender={someone.Sender} reloadSearch={this.props.reload}/>
            );
        }
        else{
            var searchList = <h1></h1>;
        }
        return(
            <div className="row d-flex justify-content-around">
                {searchList}
            </div>
        );
    }
});