var express = require('express');
var router = express.Router();
var db = require("../dbhelper");
var format = require('python-format');
var jwt = require('jsonwebtoken');
var config = require('../config');

function getChatPk(user1Id, user2Id) { // PK=Primary Key for chat
    if (user1Id < user2Id) { //In database chat is sorted by username so first in order is User1
        return [user1Id, user2Id];
    }
    else {
        return [user2Id, user1Id];
    }
};

function verifyConnection(user1, user2) { //verifying friendship for sending more messages in chat
    var query = format("SELECT * FROM connections WHERE (User1='{0}' AND User2='{1}') OR (User1='{1}' AND User2='{0}')", user1, user2);
    return db.get(query, [], (err, row) => {
        if (err) {
            console.log(err.message);
        }
        if (row && row.Confirmed == 1) {
            return true;
        }
        return false;
    });
}

/* get all friends of specific user by his username. */
router.get('/friendsList/', function (req, res) {
    jwt.verify(req.cookies.token, config.secret, function (err, decoded) { //validating token in cookie
        if (err) {
            console.log(err);
            res.sendStatus(400);
        }
        console.log(decoded)
        var userId = decoded.username;
        var query = "SELECT users.Username, users.FirstName, users.LastName, users.photoId, users.About, connections.Confirmed FROM users JOIN connections WHERE (connections.User1='" + userId + "' AND users.Username=connections.User2 AND connections.Confirmed=1) OR (connections.User2='" + userId + "' AND users.Username=connections.User1 AND connections.Confirmed=1) COLLATE NOCASE";
        db.all(query, [], (err, rows) => {
            if (err) {
                console.log(err.message);
                res.sendStatus(500);
            }
            res.send(rows);
        });
    });
});

/* get all friends requests of specific user by his username. */
router.get('/friendsRequests/', function (req, res) {
    jwt.verify(req.cookies.token, config.secret, function (err, decoded) { //validating token in cookie
        if (err) {
            console.log(err);
            res.sendStatus(400);
        }
        var userId = decoded.username;
        var query = "SELECT users.Username, users.FirstName, users.LastName, users.photoId, users.About, connections.Confirmed FROM users JOIN connections WHERE connections.Sender<>'" + userId + "' AND ((connections.User1='" + userId + "' AND users.Username=connections.User2 AND connections.Confirmed=0) OR (connections.User2='" + userId + "' AND users.Username=connections.User1 AND connections.Confirmed=0)) COLLATE NOCASE";
        db.all(query, [], (err, rows) => {
            if (err) {
                console.log(err.message);
                res.sendStatus(500);
            }
            res.send(rows);
        });
    });
});

/* get all chat information between user1 and user2 */
router.get('/chat/:usernameOtherSide', function (req, res) {
    jwt.verify(req.cookies.token, config.secret, function (err, decoded) { //validating token in cookie
        if (err) {
            console.log(err);
            res.sendStatus(400);
        }
        var users = getChatPk(decoded.username, req.params.usernameOtherSide);
        var user1 = users[0];
        var user2 = users[1];
        var query = format("SELECT * from chat WHERE (User1='{0}' and User2='{1}') OR (User1='{1}' and User2='{0}') COLLATE NOCASE", user1, user2);
        db.all(query, [], (err, rows) => {
            if (err) {
                console.log(err.message);
                res.sendStatus(500);
            }
            console.log(rows);
            res.send([rows]);
        });
    });
});

/* post new chat messages between users */
router.post('/chat/:usernameOtherSide', function (req, res) {
    jwt.verify(req.cookies.token, config.secret, function (err, decoded) { //validating token in cookie
        if (err) {
            console.log(err);
            res.sendStatus(400);
        }
        var users = getChatPk(decoded.username, req.params.usernameOtherSide);
        if (verifyConnection(decoded.username, req.params.usernameOtherSide)) { //When not friends you can't send message but only look at what already sent before
            var query = format("INSERT INTO chat (user1, user2, Content, Time) VALUES ('{0}', '{1}', '{2}', CURRENT_TIMESTAMP)", decoded.username, req.params.usernameOtherSide, req.body.content); //Insert message to SQL
            console.log(query);
            db.run(query, [], function (err) {
                if (err) {
                    console.log(err.message);
                    res.sendStatus(500);
                }
                else { res.sendStatus(200); }
            });
        }
        else {
            res.sendStatus(403); //return spoofing is known
        }
    });
});

/*get all users by search term. searching in first name or last names the term*/
router.get('/search/:term', function (req, res) {
    jwt.verify(req.cookies.token, config.secret, function (err, decoded) { //validating token in cookie
        if (err) {
            return res.sendStatus(400);
        }
        var query = format("SELECT users.Username, users.FirstName, users.LastName, users.photoId, users.About, connections.User1, connections.User2, connections.Sender, connections.Confirmed FROM users LEFT JOIN connections ON (users.Username=connections.User1 and connections.User2='{1}') or (users.Username=connections.User2 and connections.User1='{1}') WHERE (users.FirstName LIKE '%{0}%' OR users.LastName LIKE '%{0}%') AND users.Username!='{1}' GROUP BY users.Username COLLATE NOCASE", req.params.term, req.cookies.username);
        db.all(query, [], (err, rows) => {
            if (err) {
                return res.sendStatus(500);
            }
            if (rows) {
                return res.send(rows);
            }
            else {
                return res.sendStatus(400);
            }
        });
    });
});

/* ask somneone to be your friend OR accept someone request OR deny someone request*/
/* denying request is also used as unfriending*/
router.post('/friendRequest/', function (req, res) {
    jwt.verify(req.cookies.token, config.secret, function (err, decoded) { //validating token in cookie
        if (err) {
            return res.sendStatus(400);
        }
        var otherSide = req.body.otherside;
        var type = req.body.type;
        var query = "";
        if (type == "accept") {
            query = format("UPDATE connections SET Confirmed=1 WHERE (User1='{0}' AND User2='{1}') OR (User2='{0}' AND User1='{1}')", otherSide, decoded.username);
        }
        else if (type == "send") {
            query = format("INSERT INTO connections (User1, User2, Confirmed, Sender) VALUES ('{0}', '{1}', {2}, '{0}')", decoded.username, otherSide, 0);
        }
        else if (type == "deny") {
            query = format("DELETE FROM connections WHERE (User1='{0}' AND User2='{1}') OR (User2='{0}' AND User1='{1}')", otherSide, decoded.username)
        }
        db.run(query, [], (err) => {
            if (err) {
                console.log(err);
                return res.sendStatus(500);
            }
            else {
                return res.sendStatus(200);
            }
        });
    });
});

module.exports = router;
