var express = require('express');
var router = express.Router();
var format = require('python-format');
var db = require('../dbhelper');
var jwt = require('jsonwebtoken');
var config = require('../config');
var bcrypt = require('bcrypt');
var multer = require('multer');
var upload = multer({ storage: config.multStorage });

/* GET home page. */

router.get('/', function (req, res, next) {
  if (req.cookies.token === undefined) { //if user doesn't have appropriate cookie, he's a guest
    return res.render('guest');
  }
  jwt.verify(req.cookies.token, config.secret, function (err, decoded) { //check the match of the cookie
    if (err || decoded.username === undefined) { //if cookie is not good->deleting cookie and sending to guest page
      res.clearCookie('token');
      return res.render('guest');
    }
    else { //cookie is good, going to main page of members
      return res.render('index');
    }
  })
});

router.get('/logout', function (req, res, next) { //in logout, cookie is removed.
  res.clearCookie("token");
  return res.redirect('/')
});

router.post('/register', upload.single('image'), function (req, res, next) { //inserting details from registration into db. if there's duplication in username/email returning error
  var salt = bcrypt.genSaltSync(2);
  bcrypt.hash(req.body.password, salt, (err, hash) => { //hashing user's password
    if (err) {
      return res.sendStatus(500);
    }
    if (req.file === undefined) {
      var imageId = "unknown.png"
    }
    else {
      var imageId = req.file.filename;
    }
    var query = format("INSERT INTO users (Username, FirstName, LastName, Email, About, PhotoId, Password) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')", req.body.username, req.body.firstname, req.body.lastname, req.body.email, req.body.about, imageId, hash);
    db.run(query, [], function (err) {
      if (err) {
        console.log(err.message);
        return res.status(400).json({ "message": err.message });
      }
      else {
        token = jwt.sign({ username: req.body.username }, //assigning token in cookie
          config.secret,
          { expiresIn: '24h' })
        res.cookie('token', token, { httpOnly: true }); //when registration is successful, user is also logged in
        res.cookie('username', req.body.username);
        return res.sendStatus(200);
      }
    });
  });
});

router.post('/login', function (req, res, next) {
  var query = format("SELECT * FROM users WHERE Username='{0}' COLLATE NOCASE", req.body.username); // selecting the row with username
  db.get(query, [], (err, row) => {
    if (err) {
      console.log(err.message);
      return res.sendStatus(500);
    }
    else {
      if (row === undefined) {
        return res.status(401).send({ message: "incorrect" });
      }
      else {
        bcrypt.compare(req.body.password, row.Password, (err, success) => { //comparing the hash of the password
          if (err) {
            console.log(err);
            return res.sendStatus(500);
          }
          if (success == true) {
            token = jwt.sign({ username: row.Username }, //assigning jwt token in cookie
              config.secret,
              { expiresIn: '24h' })
            res.cookie('token', token, { httpOnly: true });
            res.cookie('username', row.Username);
            return res.render('index');
          }
          else {
            return res.sendStatus(401);
          }
        })
      }
    }
  });
});

module.exports = router;
